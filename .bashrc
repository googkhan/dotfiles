#   SADECE APPEND DIKKAT
EDITOR=vim

alias cp="cp -i"
alias df="df -h"
alias free="free -m"
alias more="less"
alias uu="apt update -y && apt dist-upgrade -y"
alias uc="yum update -y"
alias ua="pacman -Syu"
alias pacmansearch="pkgfile"
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

#date
#curl wttr.in/?0
#
#if command -v tmux &> /dev/null && [ -z "$TMUX" ]; then
#    tmux attach -t default || tmux new -s default
#fi
